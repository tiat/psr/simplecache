<?php
/**
 * PHP FIG
 *
 * @category     PSR-16
 * @package      SimpleCache
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\SimpleCache;

/**
 * Interface used for all types of exceptions thrown by the implementing library.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface CacheException extends Psr\SimpleCache\CacheException {

}

<?php
/**
 * PHP FIG
 *
 * @category     PSR-16
 * @package      SimpleCache
 * @license      MIT. See also the LICENCE
 */
namespace Tiat\Psr\SimpleCache;

/**
 * Exception interface for invalid cache arguments.
 * When an invalid argument is passed, it must throw an exception which implements
 * this interface.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface InvalidArgumentException extends CacheException {

}
